<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'db_gpland');

/** MySQL database username */
define('DB_USER', 'wordpress');

/** MySQL database password */
define('DB_PASSWORD', '01234830208');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '.sf<iG :%v+-l(.u NGlZl-2IF-]l-$&M#20#(|P+J?7--qNrwVkuz:81Y<qF(q^');
define('SECURE_AUTH_KEY',  '(?/HhH+Q$&eqTT%!!g-!E/,>$I%y<zbA5*7e95:}X+?s+?tU:.TS@{#+R.>{khCg');
define('LOGGED_IN_KEY',    '~;~&+8(l,dxg]z&}N5<j<c&oB<9NmOLw?V-OYzwe|vK4Rs7Y:sBY}e3F^ErZ^OwK');
define('NONCE_KEY',        'fefGQneF^Z~XTv8@FT|A-M@r0L-j<d=s]rMdc*+G7t$:gsM}M{B&WKC}lLPM=%g:');
define('AUTH_SALT',        'n#_lh;mGgG&{jx;WFmgp6$`;s|Y=Lp}:oq:KV #,+i-{-cdjijfOJm`Rp|t5f3AG');
define('SECURE_AUTH_SALT', '|t: T-9uV^$v*mcc@&~5WMS7:iNH7h!_2j|xasAO;=+{rf0*Ev)=-SNkFCuP7AUW');
define('LOGGED_IN_SALT',   '4-::A|NME,(UpwydT3O+3dM+y]+NI*zEv~sn*k4mg7:CyQ+F??/^R)C*>L5.wOD#');
define('NONCE_SALT',       '-|?/k$+|ZC(#%o~YJn&0d/Yak.]W,y7ddTuH8DQ-y0J(&K=r4DxMU!{9}dMi>HsA');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
